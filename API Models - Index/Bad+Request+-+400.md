# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  badRequest |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | badRequest | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | Generic / unknown error. Should also be delivered in case of input payload fails business logic validation | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "badRequest",
  "message": "Generic / unknown error. Should also be delivered in case of input payload fails business logic validation",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Bad Request - 400",
  "description": "",
  "type": "object",
  "properties": {
    "error": {
      "title": "Bad Request Error",
      "type": "string"
    },
    "message": {
      "title": "Bad Request Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

