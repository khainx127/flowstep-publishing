# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  unsupportedMediaType |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | unsupportedMediaType | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | Clients sends request body without content type | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "unsupportedMediaType",
  "message": "Clients sends request body without content type",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Unsupported Media Type - 415",
  "type": "object",
  "properties": {
    "error": {
      "title": "Unsupported Media Type Error",
      "type": "string"
    },
    "message": {
      "title": "Unsupported Media Type Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

