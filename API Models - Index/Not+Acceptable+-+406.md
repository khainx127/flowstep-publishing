# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  notAcceptable |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | notAcceptable | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | Resource can only generate content not acceptable according to the Accept headers sent in the request | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "notAcceptable",
  "message": "Resource can only generate content not acceptable according to the Accept headers sent in the request",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Not Acceptable - 406",
  "type": "object",
  "properties": {
    "error": {
      "title": "Not Acceptable Error",
      "type": "string"
    },
    "message": {
      "title": "Not Acceptable Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

