# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  dog |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; dog_id |  | 1 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; dog_name |  | test | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; dog_photo |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; dog_photo_id |  | 1 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; dog_photo_name |  | aaaa | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "dog_id": 1,
  "dog_name": "test",
  "dog_photo": {
    "dog_photo_id": 1,
    "dog_photo_name": "aaaa"
  }
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Dog",
  "description": "",
  "type": "object",
  "properties": {
    "dog_id": {
      "title": "Dog Id",
      "type": "integer"
    },
    "dog_name": {
      "title": "dog name",
      "type": "string"
    },
    "dog_photo": {
      "title": "Dog Photo",
      "type": "object",
      "properties": {
        "dog_photo_id": {
          "title": "Dog Photo Id",
          "type": "integer"
        },
        "dog_photo_name": {
          "title": "Dog Photo Name",
          "type": "string"
        }
      }
    }
  }
}
```

