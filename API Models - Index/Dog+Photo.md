# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  dog_photo |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; dog_photo_id |  | 1 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| &gt; dog_photo_name |  | aaaa | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |





```
{
  "dog_photo_id": 1,
  "dog_photo_name": "aaaa"
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Dog Photo",
  "type": "object",
  "properties": {
    "dog_photo_id": {
      "title": "Dog Photo Id",
      "type": "integer"
    },
    "dog_photo_name": {
      "title": "Dog Photo Name",
      "type": "string"
    }
  }
}
```

