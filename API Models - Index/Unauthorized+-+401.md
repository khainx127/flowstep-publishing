# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  unauthorized |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | unauthorized | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | The users must log in (this often means &#x27;Unauthenticated&#x27;) | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "unauthorized",
  "message": "The users must log in (this often means \u0027Unauthenticated\u0027)",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Unauthorized - 401",
  "type": "object",
  "properties": {
    "error": {
      "title": "Unauthorized Error",
      "type": "string"
    },
    "message": {
      "title": "Unauthorized Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

