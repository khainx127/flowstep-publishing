# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  preconditionFailed |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | preconditionFailed | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | Returned for conditional requests, e.g. If-Match if the condition failed. Used for optimistic locking | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "preconditionFailed",
  "message": "Returned for conditional requests, e.g. If-Match if the condition failed. Used for optimistic locking",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Precondition Failed - 412",
  "type": "object",
  "properties": {
    "error": {
      "title": "Precondition Failed Error",
      "type": "string"
    },
    "message": {
      "title": "Precondition Failed Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

