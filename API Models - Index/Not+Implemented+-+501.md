# Model Definition
| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  notImplemented |  |  -  | No | No | No | No |  |Data Type : object<br>  |
| &gt; error |  | notImplemented | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; message |  | Server cannot fulfill the request (usually implies future availability, e.g. new feature) | No | No | No | No |  |Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| &gt; code |  | 400 | No | No | No | No |  |Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |





```
{
  "error": "notImplemented",
  "message": "Server cannot fulfill the request (usually implies future availability, e.g. new feature)",
  "code": 400
}
```




```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Not Implemented - 501",
  "type": "object",
  "properties": {
    "error": {
      "title": "Not Implemented Error",
      "type": "string"
    },
    "message": {
      "title": "Not Implemented Message",
      "type": "string"
    },
    "code": {
      "title": "Http Error Code",
      "type": "integer"
    }
  }
}
```

