# Get all dogs - GET ALL

GET /dogs

## Description



* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Get|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Dogs                                           |
| Api Version                           | 1.0.0.7                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /dogs                                     |
| Resource                              | Get all dogs                                      |
| Endpoint URL                          | https://api-dev.tc1906a.com/dogs              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Accept |  | vnd.xm.device&#x2B;json;version=16 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |


---

### Query Params



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| --------- | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| limit |  | 20 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| offset |  | 0 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |


---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| requestPayload |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |



#### Json sample
```
{}
```


#### Json Schema
```
{
  "title": "Request Payload",
  "type": "object"
}
```

---

## Response Payloads

# 200

| Resource                              | Get all dogs |
| ------------------------------------- | ----------------------------------------------- |
| Path                                  |  |
| Response ClientCode                   | 200 |

## Meaning

-

## Response


#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| responsePayload |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >total |  | 15 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >items |  |  -  | No | No | No | No |  -  | Data Type : array<br>  |
| >>dog |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>>dog_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >>>dog_name |  | test | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >>>dog_photo |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>>>dog_photo_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >>>>dog_photo_name |  | aaaa | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "total": 15,
  "items": [
    {
      "dog_id": 1,
      "dog_name": "test",
      "dog_photo": {
        "dog_photo_id": 1,
        "dog_photo_name": "aaaa"
      }
    }
  ]
}
```


#### Json Schema
```
{
  "title": "Response Payload",
  "type": "object",
  "properties": {
    "total": {
      "title": "Total",
      "type": "integer"
    },
    "items": {
      "title": "Items",
      "type": "array",
      "items": {
        "title": "Dog",
        "description": "",
        "type": "object",
        "properties": {
          "dog_id": {
            "title": "Dog Id",
            "type": "integer"
          },
          "dog_name": {
            "title": "dog name",
            "type": "string"
          },
          "dog_photo": {
            "title": "Dog Photo",
            "type": "object",
            "properties": {
              "dog_photo_id": {
                "title": "Dog Photo Id",
                "type": "integer"
              },
              "dog_photo_name": {
                "title": "Dog Photo Name",
                "type": "string"
              }
            }
          }
        }
      }
    }
  }
}
```
