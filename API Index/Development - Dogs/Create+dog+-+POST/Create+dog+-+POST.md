# Create dog - POST

POST /dogs

## Description



* [Request Payloads](#request-payloads)
* [Response Payloads](#response-payloads)

| HTTP Method                           | Post|
| ------------------------------------- | ----------------------------------------------- |
| API                                   | Dogs                                           |
| Api Version                           | 1.0.0.7                                         |
| Resource Version                      | 1                                               |
| Summary                               |                                       |
| Base Path                             | /dogs                                     |
| Resource                              | Create dog                                      |
| Endpoint URL                          | https://api-dev.tc1906a.com/dogs              |
| Service Status                        |  -                                          |
| Legislative / Regulatory / Compliance |                                             |
| Firewalls Details                     |                                              |
| Security Certificate Details          |                                              |
| Vendor or Partner Considerations      |                                             |

## Request Payloads

### Request Header



| Header | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| ------ | :---------: | :----: | :-: | :-------: | :---------------: | :-------: | :-----: | ------- |
| Accept |  | vnd.xm.device&#x2B;json;version=16 | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |


---

### Query Params


N/A
---

### Request Body

#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| dog |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >dog_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >dog_name |  | test | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >dog_photo |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>dog_photo_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >>dog_photo_name |  | aaaa | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "dog_id": 1,
  "dog_name": "test",
  "dog_photo": {
    "dog_photo_id": 1,
    "dog_photo_name": "aaaa"
  }
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Dog",
  "description": "",
  "type": "object",
  "properties": {
    "dog_id": {
      "title": "Dog Id",
      "type": "integer"
    },
    "dog_name": {
      "title": "dog name",
      "type": "string"
    },
    "dog_photo": {
      "title": "Dog Photo",
      "type": "object",
      "properties": {
        "dog_photo_id": {
          "title": "Dog Photo Id",
          "type": "integer"
        },
        "dog_photo_name": {
          "title": "Dog Photo Name",
          "type": "string"
        }
      }
    }
  }
}
```

---

## Response Payloads

# 201

| Resource                              | Create dog |
| ------------------------------------- | ----------------------------------------------- |
| Path                                  |  |
| Response ClientCode                   | 201 |

## Meaning

-

## Response


#### Payload 



| Parameter | Description | Sample | PII | Sensitive | Unique Identifier | Mandatory | Default | Details |
| :----- | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: | :----- |
| dog |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >dog_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >dog_name |  | test | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |
| >dog_photo |  |  -  | No | No | No | No |  -  | Data Type : object<br>  |
| >>dog_photo_id |  | 1 | No | No | No | No |  -  | Data Type : integer<br> Minimum :  - <br> Exclusive Minimum : No<br> Maximum :  - <br> Exclusive Maximum : No<br> Multiple Of :  - <br>  |
| >>dog_photo_name |  | aaaa | No | No | No | No |  -  | Data Type : string<br> Min. length :  - <br> Max. length :  - <br> Regex :  - <br>  |



#### Json sample
```
{
  "dog_id": 1,
  "dog_name": "test",
  "dog_photo": {
    "dog_photo_id": 1,
    "dog_photo_name": "aaaa"
  }
}
```


#### Json Schema
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Dog",
  "description": "",
  "type": "object",
  "properties": {
    "dog_id": {
      "title": "Dog Id",
      "type": "integer"
    },
    "dog_name": {
      "title": "dog name",
      "type": "string"
    },
    "dog_photo": {
      "title": "Dog Photo",
      "type": "object",
      "properties": {
        "dog_photo_id": {
          "title": "Dog Photo Id",
          "type": "integer"
        },
        "dog_photo_name": {
          "title": "Dog Photo Name",
          "type": "string"
        }
      }
    }
  }
}
```
